data "google_compute_lb_ip_ranges" "ranges" {
}

locals {
  # We cannot do this inline, as we cannot nest interpolated ifs
  public_record = merge(
    {
      # Because this block also gets executed for internal LBs, we need to have a fallback
      "records" = try([google_compute_address.default[0].address], [])
      "ttl"     = 300,
    },
    (var.spectrum_config == null ? {} : { "spectrum_config" = var.spectrum_config })
  )
}

module "dns_record_external" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = var.gitlab_zone
  a = var.load_balancing_scheme == "EXTERNAL" ? { for idx, fqdn in var.fqdns :
    fqdn => local.public_record
  } : {}
}

module "dns_record_internal" {
  source = "git::git@ops.gitlab.net:gitlab-com/gl-infra/terraform-modules/dns-record.git"
  zone   = var.gitlab_zone

  # Annotation by Hendrik: Todo copied from old aws_route53_record resource (previous commit 0faf5f7)
  # TODO craigf: see description on forwarding rule below.
  # Since we can't create the forwarding rule, we can't create a record based on
  # its frontend IP, therefore we implicitly exclude INTERNAL_MANAGED.
  a = var.load_balancing_scheme == "INTERNAL" ? { for idx, fqdn in var.fqdns :
    fqdn => {
      "records" = [google_compute_forwarding_rule.internal[0].ip_address]
      "ttl"     = 300
      # There is no spectrum config on internal loadbalancers, as that would not work.
    }
  } : {}
}

resource "google_compute_address" "default" {
  count = var.load_balancing_scheme == "INTERNAL_MANAGED" ? 0 : 1

  name         = format("%v-%v", var.environment, var.name)
  project      = var.project
  region       = var.region
  address_type = var.load_balancing_scheme == "EXTERNAL" ? "EXTERNAL" : "INTERNAL"
}

resource "google_compute_firewall" "default" {
  name    = format("%v-%v", var.environment, var.name)
  network = var.network == null ? var.environment : var.network

  allow {
    protocol = "tcp"
    ports    = var.health_check_ports
  }

  source_ranges = concat(
    data.google_compute_lb_ip_ranges.ranges.network,
    data.google_compute_lb_ip_ranges.ranges.http_ssl_tcp_internal,
  )
  target_tags = var.targets
}

resource "google_compute_forwarding_rule" "default" {
  count = var.load_balancing_scheme == "EXTERNAL" ? var.lb_count : 0
  name = format(
    "%v-%v-%v",
    var.environment,
    var.name,
    var.names[count.index],
  )
  project               = var.project
  region                = var.region
  target                = google_compute_target_pool.default[count.index].self_link
  load_balancing_scheme = var.load_balancing_scheme
  port_range            = var.forwarding_port_ranges[count.index]
  ip_address            = google_compute_address.default[0].address
}

resource "google_compute_forwarding_rule" "internal" {
  # TODO craigf: until a fix for
  # https://github.com/terraform-providers/terraform-provider-google/issues/4935
  # is released, we cannot create INTERNAL_MANAGED forwarding rules that point
  # to target proxies that point to INTERNAL_MANAGED backend services, for
  # internal HTTP load balancing.
  # It's likely this resource already exists, and when the fix is released and
  # this module is updated to support it, we will have to `terraform import`
  # them.
  count = var.load_balancing_scheme == "INTERNAL" ? 1 : 0

  name                  = format("%v-%v", var.environment, var.name)
  project               = var.project
  region                = var.region
  backend_service       = var.backend_service
  load_balancing_scheme = var.load_balancing_scheme
  ports                 = var.forwarding_port_ranges
  network               = var.network == null ? var.environment : var.network
  service_label         = "i"
  subnetwork            = var.subnetwork_self_link

  # TODO
  # subnetwork = proxy-only-subnet
  # target = INTERNAL_MANAGED http proxy below
  # Conditionalise backend_service vs http proxy, or if that's hard/ugly, make
  # another forwarding_rule declaration below.
}

resource "google_compute_target_pool" "default" {
  count = var.load_balancing_scheme == "EXTERNAL" ? var.lb_count : 0
  name = format(
    "%v-%v-%v",
    var.environment,
    var.name,
    var.names[count.index],
  )
  project          = var.project
  region           = var.region
  session_affinity = var.session_affinity
  instances        = var.instances

  health_checks = [
    google_compute_http_health_check.default[count.index].self_link,
  ]
}

resource "google_compute_http_health_check" "default" {
  count = var.lb_count
  name = format(
    "%v-%v-%v",
    var.environment,
    var.name,
    var.names[count.index],
  )
  project = var.project
  port    = var.health_check_ports[count.index]

  # Because request_paths can be empty, we use this element/concat hack, see https://stackoverflow.com/a/47415781/1856239
  request_path        = length(var.health_check_request_paths) > 0 ? element(concat(var.health_check_request_paths, [""]), count.index) : format("/-/available-%v", var.names[count.index])
  timeout_sec         = 2
  check_interval_sec  = 2
  healthy_threshold   = 2
  unhealthy_threshold = 2
}

resource "google_compute_region_target_http_proxy" "default" {
  provider = google-beta

  count = var.load_balancing_scheme == "INTERNAL_MANAGED" ? var.lb_count : 0

  name = format(
    "%v-%v-%v",
    var.environment,
    var.name,
    var.names[count.index],
  )

  region  = var.region
  url_map = google_compute_region_url_map.default[count.index].self_link
}

resource "google_compute_region_url_map" "default" {
  provider = google-beta

  count = var.load_balancing_scheme == "INTERNAL_MANAGED" ? var.lb_count : 0

  name = format(
    "%v-%v-%v",
    var.environment,
    var.name,
    var.names[count.index],
  )

  default_service = var.backend_service
  region          = var.region
}

output "address" {
  value = var.load_balancing_scheme == "EXTERNAL" ? google_compute_address.default[0].address : try(google_compute_forwarding_rule.internal[0].ip_address, "")
}
