variable "name" {}
variable "environment" {}
variable "project" {}
variable "region" {}
variable "zone" {}
variable "service_account_ci" {}
variable "vpc" {}
variable "subnet" {}

variable "default_disk_size" { default = "100" }
variable "default_disk_type" { default = "pd-standard" }

variable "geo_site" { default = null }
variable "geo_deployment" { default = null }

variable "machine_image" { default = "ubuntu-1804-lts" }
variable "service_account_roles" { default = ["roles/editor"] }

variable "postgres_node_count" {
  default = 2
}

variable "postgres_disks" {
  default = [
    {
      size        = 100
      type        = "pd-ssd"
      device_name = "data"
    },
    {
      size        = 50
      type        = "pd-standard"
      device_name = "log"
    },
  ]
}

variable "pgbouncer_node_count" {
  default = 3
}

variable "pgbouncer_disks" {
  default = [
    {
      size        = 50
      type        = "pd-standard"
      device_name = "log"
    },
  ]
}

variable "postgres_machine_type" {
  default = "n1-standard-4"
}
variable "pgbouncer_machine_type" {
  default = "n1-standard-4"
}

variable "object_storage_buckets" {
  default = []
}

variable "secrets" {
  default = []
}
