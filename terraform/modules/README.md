# Terraform modules

This directory contains modules that are used by Terraform.

In order to initialize Terraform you must create a symlink to the [GitLab Environment Toolkit](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/) module drectory.
See the project README for local setup.
