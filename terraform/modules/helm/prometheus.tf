resource "helm_release" "kube-prometheus-stack" {
  name = "kube-prometheus-stack"

  repository       = "https://prometheus-community.github.io/helm-charts"
  chart            = "kube-prometheus-stack"
  version          = "17.2.2"
  namespace        = "monitoring"
  create_namespace = true
  wait             = false

  values = [
    "${file("${path.module}/kube-prometheus-stack-values.yaml")}"
  ]
  depends_on = [
    kubernetes_service.consul_http
  ]
}
