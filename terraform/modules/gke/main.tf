data "google_service_account_access_token" "kubernetes_sa" {
  target_service_account = "${var.service_account_ci}@${var.project}.iam.gserviceaccount.com"
  scopes                 = ["userinfo-email", "cloud-platform"]
  lifetime               = "3600s"
}

module "gitlab-gke" {
  environment = var.environment
  name        = "gitlab-gke"
  # VPC will be updated when we migrate off of the default VPC
  vpc                    = "https://www.googleapis.com/compute/v1/projects/${var.project}/global/networks/default"
  source                 = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/gke.git?ref=v11.4.3"
  ip_cidr_range          = var.gke_subnetworks["gitlab-gke"]
  disable_network_policy = "false"
  dns_zone_name          = "gitlab.net"
  kubernetes_version     = "1.20"
  maintenance_policy = {
    start_time = "2020-09-14T02:00:00Z"
    end_time   = "2020-09-14T08:00:00Z"
    recurrence = "FREQ=WEEKLY;BYDAY=MO,TU"
  }
  private_master_cidr   = "172.16.0.0/28"
  project               = var.project
  region                = var.region
  release_channel       = "REGULAR"
  pod_ip_cidr_range     = var.gke_subnetworks["gitlab-gke-pod-cidr"]
  service_ip_cidr_range = var.gke_subnetworks["gitlab-gke-service-cidr"]
  subnet_nat_name       = "${var.environment}-nat"
  workload_identity_ksa = {
    # consul needs to be able to list instances
    # to auto-join the omnibus consul cluster
    "consul/consul-consul-client" = ["roles/compute.admin"]
  }
  node_pools = {
    "default-1" = {
      node_disk_size_gb = "50"
      node_disk_type    = "pd-standard"
      node_auto_upgrade = true
      preemptible       = "true"
      type              = "default"
    }
  }
}
