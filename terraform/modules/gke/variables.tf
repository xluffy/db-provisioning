variable "project" {}
variable "region" {}
variable "environment" {}
variable "service_account_ci" {}

variable "gke_subnetworks" {
  default = {
    # default vpc values, these should be overridden if using a different VPC
    "gitlab-gke-pod-cidr"     = "10.1.0.0/18"   # 10.1.0.0 - 10.1.63.255 (16384 pods)
    "gitlab-gke-service-cidr" = "10.1.64.0/18"  # 10.1.64.0 - 10.1.127.255 (16384 services)
    "gitlab-gke"              = "10.1.128.0/23" # 10.1.128.0 - 10.1.129.255 (512 nodes)
  }
}
