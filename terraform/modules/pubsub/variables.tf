variable "project" {
  type        = string
  description = "The project name"
}

variable "environment" {
  type        = string
  description = "The environment name"
}

variable "pubsub_topics" {
  type        = list(string)
  description =  "The list of Pub/Sub topics to create"
  default     = []
}
