variable "vpc" {}
variable "subnet" {}

variable "node_count" {
  default = 1
}

variable "prefix" {
  type = string
}

variable "name" {
  default = "pgbouncer"
}

variable "region" {
  type = string
}

variable "environment" {
  type = string
}

variable "project" {
  type = string
}

variable "machine_type" {
  default = "n1-standard-4"
}

variable "machine_image" {
  default = "ubuntu-1804-lts"
}

variable "disks" {
  default = []
}

variable "geo_site" {
  default = null
}

variable "geo_deployment" {
  default = null
}
