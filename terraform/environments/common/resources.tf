terraform {
  backend "http" {
  }
}

provider "google" {
  region  = var.region
  zone    = var.zone
  project = var.project
}

# Using `tcp-lb` module (for pgbouncer ILB) forces us to configure
# the aws and cloudflare providers, even though we're not planning
# on using them. Hence we set the region for aws and some dummy credentials
# for cloudflare. This is temporary till GET supports GCP LBs natively.
provider "cloudflare" {
  email      = "dummy@example.com"
  api_key    = "0000000000000000000000000000000000000"
  account_id = "0000000000000000000000000000000000000"
}

provider "aws" {
  region                      = "us-east-1"
  access_key                  = "00000000000000000000"
  secret_key                  = "0000000000000000000000000000000000000000"
  skip_credentials_validation = true
  skip_requesting_account_id  = true
}

provider "helm" {
  kubernetes {
    host  = "https://${module.gke.cluster_endpoint}"
    token = module.gke.access_token

    cluster_ca_certificate = base64decode(
      module.gke.cluster_ca_certificate
    )
  }
}

provider "kubernetes" {
  host  = "https://${module.gke.cluster_endpoint}"
  token = module.gke.access_token
  cluster_ca_certificate = base64decode(
    module.gke.cluster_ca_certificate
  )
}

# Environment specific resources

module "network" {
  count            = var.existing_vpc_name == "" ? 1 : 0
  environment      = var.environment
  project          = var.project
  source           = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/vpc.git?ref=v2.0.4"
  internal_subnets = [var.internal_subnet]
}

data "google_compute_network" "network" {
  count = var.existing_vpc_name == "" ? 0 : 1
  name  = var.existing_vpc_name
}

locals {
  vpc = var.existing_vpc_name == "" ? module.network[0].self_link : data.google_compute_network.network[0].self_link
}

resource "google_compute_subnetwork" "subnetwork" {
  name                     = format("%v-db", var.environment)
  network                  = local.vpc
  project                  = var.project
  region                   = var.region
  ip_cidr_range            = var.internal_subnet
  private_ip_google_access = true
}
