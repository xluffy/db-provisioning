variable "region" {
  default = "us-east1"
}

variable "zone" {
  default = "us-east1-b"
}

variable "service_account_ci" {
  default = "terraform-ci"
}

variable "iam_binding_roles" {
  default = [
    "roles/compute.instanceAdmin",
    "roles/compute.instanceAdmin.v1",
    "roles/compute.osAdminLogin",
    "roles/iam.serviceAccountUser",
  ]
}
