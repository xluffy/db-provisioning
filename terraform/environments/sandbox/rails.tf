module "rails" {
  prefix            = var.environment
  node_type         = "rails"
  node_count        = 1
  machine_type      = "n1-standard-8"
  machine_image     = "ubuntu-1804-lts"
  disk_size         = 100
  disk_type         = "pd-ssd"
  setup_external_ip = false
  source            = "../../modules/get/gitlab_gcp_instance"
  additional_labels = {
    "gitlab_env" : var.environment,
  }
}
