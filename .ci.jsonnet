local woodhouseImg = "registry.gitlab.com/gitlab-com/gl-infra/woodhouse:latest";
local shards = import 'shards.libsonnet';
// Conditions

local cond = {
  ifNotTag: { "if": "$CI_COMMIT_TAG", when: "never" },
  ifNotNonDefaultBranch: { "if": "$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH", when: "never" },
  ifNotDefaultBranch: { "if": "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH", when: "never" },
  ifMirror: { "if": "$MIRROR == \"true\"" },
  ifMirrorManual: { "if": "$MIRROR == \"true\"", when: "manual" },
  ifCanonical: { "if": "$CANONICAL == \"true\"" },
};

// Rules

local rules = {
  Canonical: { rules: [cond.ifNotTag, cond.ifCanonical] },
  Mirror: { rules: [cond.ifNotTag, cond.ifMirror] },
  MirrorManual: { rules: [cond.ifNotTag, cond.ifMirrorManual] },
  MirrorDefaultBranch: { rules: [cond.ifNotTag, cond.ifNotNonDefaultBranch, cond.ifMirror] },
  MirrorDefaultBranchManual: { rules: [cond.ifNotTag, cond.ifNotNonDefaultBranch, cond.ifMirror] },
};

// Template functions

local image(name) = {
  image: "docker:19.03.8",
  stage: "Image",
  allow_failure: true,
  variables: {
    DOCKER_DRIVER: "overlay2",
    DOCKER_HOST: "tcp://docker:2375",
  },
  services: ["docker:19.03.5-dind"],
  script: [
    "echo ${CI_JOB_TOKEN} | docker login --password-stdin -u $CI_REGISTRY_USER $CI_REGISTRY",
    "docker build -t $CI_REGISTRY_IMAGE/ci-%s:latest --tag $CI_REGISTRY_IMAGE/ci-%s:$CI_COMMIT_SHORT_SHA -f docker/Dockerfile-ci-%s ." % [name, name, name],
    "docker push $CI_REGISTRY_IMAGE/ci-%s:$CI_COMMIT_SHORT_SHA" % name,
    "docker push $CI_REGISTRY_IMAGE/ci-%s:latest" % name,
  ],
};

local envTerraform(env) = {
  image: "${CI_REGISTRY_IMAGE}/ci-tf:latest",
  script: [
    "cd $CI_PROJECT_DIR/terraform/environments/%s" % env,
  ],
  variables: {
    TF_ROOT: "${CI_PROJECT_DIR}/terraform/environments/%s" % env,
    TF_ADDRESS: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/%s" % env,
  },
  cache: {
    key: "%s-terraform" % env,
    paths: [
      "${TF_ROOT}/.terraform",
    ],
  },
  environment: {
    name: env,
  },
};

local envAnsible(env, play, shard="") = {
  local inventory = if shard == "" then "environments/%s/inventory" % env else "environments/%s/shards/%s/inventory" % [env, shard],
  local playFname = "%s.yml" % play,
  image: "${CI_REGISTRY_IMAGE}/ci-ansible:latest",
  script: [
    // KNIFE_RB_FILE expects the client key to be in the same directory
    "cp $CHEF_KEY_FILE $(dirname $KNIFE_RB_FILE)/client.pem",
    "cd $CI_PROJECT_DIR/ansible",
    // Necessary for Ansible inventory
    // Note tha this cannot be set in a `variables:` block as it will set
    // the variable to the file's contents, instead of the file path.
    "export GCP_SERVICE_ACCOUNT_FILE=$GOOGLE_APPLICATION_CREDENTIALS",
    // To avoid the "Ansible is being run in a world writable" directory error
    "export ANSIBLE_CONFIG=ansible.cfg",
    // Load mitogen from the path where it got installed on by `pip`
    "export ANSIBLE_STRATEGY_PLUGINS=/asdf/.asdf/installs/python/3.9.6/lib/python3.9/site-packages/ansible_mitogen/plugins/strategy/",
    'echo "Running: ansible-playbook -i group_vars/get -i %s %s"' % [inventory, playFname],
    "ansible-playbook -i group_vars/get -i %s %s" % [inventory, playFname],
  ],
  allow_failure: true,
  environment: {
    name: env,
  },
  variables: {
    // Default to nightly builds until we switch to the official release channel
    GITLAB_REPO_SCRIPT_URL: "https://packages.gitlab.com/install/repositories/gitlab/nightly-builds/script.deb.sh",
  },
};

// Jobs for the primary pipeline
// --------------------------------
// These functions are for the main pipeline, up until
// the environment trigges

{
  imageCIJob(name):: {
    ["image:%s" % name]: rules.MirrorManual + image(name),
  },

  notifyMirrorSourceJob():: {
    "notify mirror source": rules.Mirror {
      stage: "Prepare",
      allow_failure: true,
      image: woodhouseImg,
      script: "woodhouse gitlab notify-mirrored-mr",
    },
  },

  lintCIJob():: {
    "ci config generated": rules.Mirror {
      stage: "Prepare",
      image: "${CI_REGISTRY_IMAGE}/ci-lint:latest",
      script: [
        "make generate-config",
        "git diff --exit-code",
      ],
    },
  },

  checkRemoteJob():: {
    "check remote": rules.Canonical {
      stage: "Check Remote",
      image: woodhouseImg,
      script: 'woodhouse gitlab follow-remote-pipeline --gitlab-api-base-url="https://ops.gitlab.net/api/v4" --gitlab-api-token="$OPS_API_TOKEN"',
    },
  },

  environmentJobs():: {
    [env]: rules.MirrorManual {
      stage: "Environments",
      allow_failure: true,
      trigger: {
        include: [
          { template: "Workflows/Branch-Pipelines.gitlab-ci.yml" },
          { "local": "/ci/%s.yml" % env },
        ],
      },
    }
    for env in std.objectFields(shards.environments)
  },

  // Jobs for environments
  // --------------------------------
  // These functions are for jobs in the child pipeline

  ansibleJobs(env):: {
    [play]: rules.MirrorManual + envAnsible(env, play) + {
      stage: env,
    }
    for play in shards.environments[env].plays
  } + {
    // We may be able to switch this to only display the play eventually, see
    // https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/24
    ["%s:%s:%s" % [play, shard, env]]: rules.MirrorManual + envAnsible(env, play, shard) + {
      stage: "Shard:%s" % shard,
    }
    for shard in std.objectFields(shards.shards)
    if [shardEnv for shardEnv in shards.shards[shard].environments if shardEnv == env] != []
    for play in shards.shards[shard].plays
  },

  tfPrepareJob(env):: {
    ["%s:prepare" % env]: rules.Mirror + envTerraform(env) + {
      stage: "Prepare",
      artifacts: {
        name: "%s-prepare" % env,
        paths: [
          "${TF_ROOT}/plan.cache",
        ],
        reports: {
          terraform: "${TF_ROOT}/plan.json",
        },
      },
      resource_group: "%s:prepare",
      script: super.script + [
        |||
          eval $(ssh-agent -s)
          echo "$SSH_PRIVATE_KEY" | base64 -d | tr -d '\r' | ssh-add - > /dev/null
          mkdir -p ~/.ssh
          chmod 700 ~/.ssh
          echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
          chmod 644 ~/.ssh/known_hosts
          echo "Initializing working directory for %s..."
        ||| % env,
        "gitlab-terraform init",
        "gitlab-terraform validate",
        "gitlab-terraform plan",
        "gitlab-terraform plan-json",
      ],
    },
  },

  tfApplyJob(env):: {
    ["%s:apply" % env]: rules.MirrorManual + envTerraform(env) + {
      stage: "Deploy",
      dependencies: ["%s:prepare" % env],
      resource_group: "%s:apply" % env,
      script: super.script + [
        "gitlab-terraform apply",
      ],
    },
  },

  tfDestroyJob(env):: {
    ["%s:destroy" % env]: rules.MirrorManual + envTerraform(env) + {
      stage: "Destroy",
      dependencies: ["%s:prepare" % env],
      resource_group: "%s:destroy" % env,
      script: super.script + [
        "gitlab-terraform destroy",
      ],
    },
  },
}
