#!/bin/bash

set -o pipefail
set -o errexit

function usage()
{
    ERROR_MESSAGE=$1
    [[ -n "$ERROR_MESSAGE" ]] && echo "Error: $ERROR_MESSAGE" && echo

    cat <<HERE
Usage: perf_flamegraph_for_pid.sh [pids]

Captures an on-CPU stack profile, and renders it as a flamegraph.
Captures only the given PID.  Can be a comma-separated list of PIDs.
HERE
    exit 1
}

function main()
{
    CHOSEN_PID=$1
    DURATION_SECONDS=60
    SAMPLES_PER_SECOND=99

    [[ $1 =~ ^-h|--help$ ]] && usage
    [[ $# -eq 1 ]] || usage "Wrong number of arguments"
    [[ "$CHOSEN_PID" =~ ^[0-9,]+$ ]] || usage "Invalid PID: '$CHOSEN_PID'"

    # Use a temp dir.  This avoids polluting current dir and supports concurrent runs of this script.
    OUTDIR=$( mktemp -d /tmp/perf-record-results.XXXXXXXX )
    cd "$OUTDIR"

    # Name the output files to clearly indicate the scope and timestamp of the capture.
    OUTFILE_PREFIX="$( hostname -s ).$( date +%Y%m%d_%H%M%S_%Z ).pid_${CHOSEN_PID}"
    OUTFILE_PERF_SCRIPT="${OUTFILE_PREFIX}.perf-script.txt.gz"
    OUTFILE_FLAMEGRAPH="${OUTFILE_PREFIX}.flamegraph.svg"

    # Capture timer-based profile, resolve symbols, and render as a flamegraph.
    echo "Starting capture for $DURATION_SECONDS seconds."
    sudo perf record --freq $SAMPLES_PER_SECOND -g --pid $CHOSEN_PID -- sleep "$DURATION_SECONDS"
    sudo perf script --header | gzip > "$OUTFILE_PERF_SCRIPT"
    zcat "$OUTFILE_PERF_SCRIPT" | stackcollapse-perf.pl --kernel | flamegraph.pl --hash --colors=perl > "$OUTFILE_FLAMEGRAPH"

    # Show user where the output is.
    echo
    echo "Results:"
    echo "Flamegraph:       ${OUTDIR}/${OUTFILE_FLAMEGRAPH}"
    echo "Raw stack traces: ${OUTDIR}/${OUTFILE_PERF_SCRIPT}"
}

main "$@"
