---

- name: Download process_exporter binary
  get_url:
    url: "{{ process_exporter_url }}"
    dest: "/var/tmp"
    checksum: "{{ process_exporter_checksum }}"
  register: process_exporter_archive
  until: process_exporter_archive is succeeded
  retries: 5
  delay: 2
  tags:
    - exporter

- name: Create process_exporter install directory
  file:
    dest: "{{ process_exporter_install_directory }}"
    state: directory
    mode: 0755
  tags:
    - exporter

- name: Extract process_exporter archive
  unarchive:
    src: "{{ process_exporter_archive.dest }}"
    dest: "{{ process_exporter_install_directory }}"
    remote_src: true
    extra_opts:
      - --strip-components=1
  notify:
    - Restart process_exporter
  tags:
    - exporter

- name: Create process_exporter systemd service
  template:
    src: process_exporter.systemd.j2
    dest: "/etc/systemd/system/process_exporter.service"
    mode: '0644'
  notify:
    - Restart process_exporter
  tags:
    - exporter

- name: Create process_exporter configuration file
  copy:
    dest: "{{ process_exporter_install_directory }}/config.yml"
    content: |
      process_names:
      {{ process_exporter_names }}
    backup: false
    mode: 0644
  when:
    - process_exporter_names != ""
  notify:
    - Restart process_exporter
  tags:
    - exporter

- name: Register Consul Prometheus Scrape Endpoints
  include_role:
    name: consul-agent
  vars:
    service_id: "{{ process_exporter_name }}"
    service_definition:
      service:
        meta: "{{ consul_service_meta | default({}) }}"
        name: "{{ process_exporter_name }}"
        address: "{{ ansible_default_ipv4.address }}"
        port: "{{ process_exporter_listen_port }}"
