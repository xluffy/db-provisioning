---

- name: Include pgbouncer INI default configuration
  include_vars:
    file: pgbouncer_ini.yml
    name: default_pgbouncer_ini

- name: Merge pgbouncer configuration
  set_fact:
    pgbouncer_ini: "{{ default_pgbouncer_ini | combine(pgbouncer_ini) }}"

- name: Check pgbouncer version
  shell:
    cmd: "test -f {{ pgbouncer_install_directory }}/bin/pgbouncer && {{ pgbouncer_install_directory }}/bin/pgbouncer --version | grep PgBouncer | cut -d' ' -f2"
  register: pgbouncer_version_check
  ignore_errors: true
  tags:
    - compile

- name: Install pgbouncer
  include_tasks: compile.yml
  when: "pgbouncer_version_check.stdout != pgbouncer_version"
  tags:
    - compile

- name: Check existing pgbouncer services
  find:
    paths: "{{ pgbouncer_data_directory }}"
    patterns: pgbouncer*.ini
  register: existing_pgbouncer_services
  tags:
    - housekeeping

- name: Create pgbouncer group
  group:
    name: "{{ pgbouncer_group }}"
  tags:
    - group

- name: Create pgbouncer user
  user:
    name: "{{ pgbouncer_user }}"
    groups: "{{ pgbouncer_group }}"
    append: true
  tags:
    - user

- name: Add PostgreSQL APT repository key
  apt_key:
    url: https://download.postgresql.org/pub/repos/apt/ACCC4CF8.asc
  tags:
    - pgbouncer-client

- name: Add PostgreSQL APT repository
  apt_repository:
    repo: "deb https://download.postgresql.org/pub/repos/apt/ {{ ansible_facts['distribution_release'] }}-pgdg main {{ postgresql_version }}"
    filename: postgresql
  tags:
    - pgbouncer-client

- name: Install pgbouncer client-related packages
  apt:
    package:
      - "postgresql-client-{{ postgresql_version }}"
      - runit
  tags:
    - pgbouncer-client

- name: Create pgbouncer data directory
  file:
    path: "{{ pgbouncer_data_directory }}"
    state: directory
    owner: "{{ pgbouncer_user }}"
    group: "{{ pgbouncer_group }}"
    mode: '0700'
  tags:
    - directories

- name: Create pgbouncer log directory
  file:
    path: "{{ pgbouncer_log_directory }}"
    state: directory
    owner: "{{ log_user }}"
    group: "{{ log_group }}"
  tags:
    - directories

- name: Create pgbouncer rsyslog configuration
  template:
    src: templates/pgbouncer.rsyslog.conf.j2
    dest: "/etc/rsyslog.d/60-pgbouncer.conf"
    owner: "{{ log_user }}"
    group: "{{ log_group }}"
  vars:
    program_name: "{{ pgbouncer_ini['syslog_ident'] }}"
    log_path: "{{ pgbouncer_log_directory }}/pgbouncer.log"
  notify:
    - Restart rsyslog
  when: "pgbouncer_ini['syslog'] == 1"
  tags:
    - logging

- name: Create pgbouncer configuration file
  template:
    src: templates/pgbouncer.ini.j2
    dest: "{{ pgbouncer_data_directory }}/pgbouncer{{ (loop_idx == 0) | ternary('', '-' ~ loop_idx) }}.ini"
    owner: "{{ pgbouncer_user }}"
    group: "{{ pgbouncer_group }}"
    mode: '0600'
  loop: "{{ pgbouncer_ini['listen_ports'] }}"
  loop_control:
    index_var: loop_idx
  vars:
    listen_port: "{{ item }}"
    pidfile: "{{ pgbouncer_data_directory }}/pgbouncer-{{ loop_idx }}.pid"
  notify:
    - Reload pgbouncer
  tags:
    - config

- name: Remove no-longer-used pgbouncer configuration files
  file:
    path: "{{ pgbouncer_data_directory }}/pgbouncer-{{ item }}.ini"
    state: absent
  loop: "{{ range(pgbouncer_ini['listen_ports']|count, existing_pgbouncer_services['matched']) | list }}"
  tags:
    - housekeeping

- name: Create pgbouncer systemd service
  template:
    src: templates/pgbouncer.systemd.j2
    dest: "/etc/systemd/system/{{ name }}.service"
    mode: '0644'
  loop: "{{ pgbouncer_ini['listen_ports'] }}"
  loop_control:
    index_var: loop_idx
  vars:
    name: "pgbouncer{{ (loop_idx == 0) | ternary('', '-' ~ loop_idx) }}"
    command: "{{ pgbouncer_install_directory }}/bin/pgbouncer {{ pgbouncer_data_directory }}/{{ name }}.ini"
  tags:
    - service

- name: Create pgbouncer console executable
  template:
    src: templates/pgb-console.j2
    dest: "/usr/local/bin/pgb-console{{ (loop_idx == 0) | ternary('', '-' ~ loop_idx) }}"
    mode: '0777'
  loop: "{{ pgbouncer_ini['listen_ports'] }}"
  loop_control:
    index_var: loop_idx
  vars:
    port: "{{ item }}"
  tags:
    - pgbouncer-client

- name: Remove no-longer-used pgbouncer console executables
  file:
    path: "/usr/local/bin/pgb-console-{{ item }}"
    state: absent
  loop: "{{ range(pgbouncer_ini['listen_ports']|count, existing_pgbouncer_services['matched']) | list }}"
  tags:
    - housekeeping

- name: Create pgbouncer authentication file
  template:
    src: templates/pg_auth.j2
    dest: "{{ pgbouncer_data_directory }}/pg_auth"
    owner: "{{ pgbouncer_user }}"
    group: "{{ pgbouncer_group }}"
    mode: "0600"
  notify:
    - Reload pgbouncer
  tags:
    - config

- name: Create pgbouncer databases file
  template:
    src: templates/databases.ini.j2
    dest: "{{ pgbouncer_ini['databases_ini'] }}"
    owner: "{{ pgbouncer_user }}"
    group: "{{ pgbouncer_group }}"
    mode: "0600"
  notify:
    - Reload pgbouncer
  tags:
    - config

- name: Stop no-longer-used pgbouncer systemd services
  service:
    name: "pgbouncer-{{ item }}.service"
    state: stopped
  loop: "{{ range(pgbouncer_ini['listen_ports']|count, existing_pgbouncer_services['matched']) | list }}"
  tags:
    - housekeeping

- name: Remove no-longer-used pgbouncer systemd services
  file:
    path: "/etc/systemd/system/pgbouncer-{{ item }}.service"
    state: absent
  loop: "{{ range(pgbouncer_ini['listen_ports']|count, existing_pgbouncer_services['matched']) | list }}"
  tags:
    - housekeeping

- name: Reload systemd daemon
  command: systemctl daemon-reload
  tags:
    - service

- name: Start pgbouncer
  service:
    name: "pgbouncer{{ (loop_idx == 0) | ternary('', '-' ~ loop_idx) }}"
    state: started
    enabled: true
  loop: "{{ pgbouncer_ini['listen_ports'] }}"
  loop_control:
    index_var: loop_idx
  tags:
    - service

- name: Install pgbouncer exporter
  include_tasks: exporter.yml
  when: pgbouncer_exporter_enabled is true
  tags:
    - exporter

- name: Setup GitLab config file
  template:
    src: templates/consul.gitlab.rb.j2
    dest: "/etc/gitlab/gitlab.rb"
  when: pgbouncer_install_consul is true
  notify:
    - Reconfigure Consul
  tags:
    - reconfigure
    - consul

- name: Run reconfigure if necessary
  meta: flush_handlers

- name: Include consul encryption configuration if enabled
  include_role:
    name: consul-encrypt
  vars:
    consul_cert: "{{ consul_client_cert }}"
    consul_key: "{{ consul_client_key }}"
  tags:
    - reconfigure
  when:
    # If the consul encyption secret still has the placeholder
    # value, do not include the role
    - not "consul_encrypt!" in consul_encrypt

- name: Create Consul service
  include_tasks: consul.yml
  tags:
    - consul

- name: Create logrotate configuration
  import_role:
    name: arillso.logrotate
  tags:
    - logging
