---
- name: Download pgbouncer_exporter archive
  get_url:
    url: "{{ pgbouncer_exporter_url }}"
    dest: "/var/tmp"
    checksum: "{{ pgbouncer_exporter_checksum }}"
  register: pgbouncer_exporter_archive
  tags:
    - exporter

- name: Create pgbouncer_exporter install directory
  file:
    dest: "{{ pgbouncer_exporter_install_directory }}"
    state: directory
  tags:
    - exporter

- name: Extract pgbouncer_exporter archive
  unarchive:
    src: "{{ pgbouncer_exporter_archive.dest }}"
    dest: "{{ pgbouncer_exporter_install_directory }}"
    remote_src: true
    extra_opts:
      - --strip-components=1
  notify:
    - Restart pgbouncer_exporter
  tags:
    - exporter

- name: Create pgbouncer_exporter systemd service
  template:
    src: templates/pgbouncer_exporter.systemd.j2
    dest: "/etc/systemd/system/{{ name }}.service"
    mode: '0644'
  loop: "{{ pgbouncer_ini['listen_ports'] }}"
  loop_control:
    index_var: loop_idx
  vars:
    name: "pgbouncer_exporter{{ (loop_idx == 0) | ternary('', '_' ~ loop_idx) }}"
    command: >-
      {{ pgbouncer_exporter_install_directory }}/pgbouncer_exporter
      --pgBouncer.connectionString="{{ pgbouncer_exporter_connection_string }} port={{ item }}"
      --pgBouncer.pid-file={{ pgbouncer_data_directory }}/pgbouncer-{{ loop_idx }}.pid
      --web.listen-address={{ pgbouncer_exporter_web_listen_address }}:{{ pgbouncer_exporter_web_listen_port + loop_idx }}
      --log.format={{ pgbouncer_exporter_log_format }}
  register: pgbouncer_exporter_services
  tags:
    - exporter
    - service

- name: Stop no-longer-used pgbouncer_exporter systemd services
  service:
    name: "pgbouncer_exporter_{{ item }}.service"
    state: stopped
  loop: "{{ range(pgbouncer_ini['listen_ports']|count, existing_pgbouncer_services['matched']) | list }}"
  tags:
    - housekeeping

- name: Remove no-longer-used pgbouncer_exporter systemd services
  file:
    path: "/etc/systemd/system/pgbouncer_exporter_{{ item }}.service"
    state: absent
  loop: "{{ range(pgbouncer_ini['listen_ports']|count, existing_pgbouncer_services['matched']) | list }}"
  register: removed_pgbouncer_exporter_services
  tags:
    - housekeeping

- name: Reload systemd daemon
  command: systemctl daemon-reload
  when: pgbouncer_exporter_services is changed or removed_pgbouncer_exporter_services is changed
  tags:
    - exporter
    - service

- name: Create pgbouncer_exporter log directory
  file:
    path: "{{ pgbouncer_exporter_log_directory }}"
    state: directory
    owner: "{{ log_user }}"
    group: "{{ log_group }}"
  tags:
    - exporter
    - directories

- name: Create pgbouncer rsyslog configuration
  template:
    src: templates/pgbouncer_exporter.rsyslog.conf.j2
    dest: "/etc/rsyslog.d/61-pgbouncer_exporter.conf"
    owner: "{{ log_user }}"
    group: "{{ log_group }}"
  notify:
    - Restart rsyslog
  tags:
    - exporter
    - logging

- name: Start pgbouncer_exporter
  service:
    name: "pgbouncer_exporter{{ (loop_idx == 0) | ternary('', '_' ~ loop_idx) }}"
    state: started
    enabled: true
  loop: "{{ pgbouncer_ini['listen_ports'] }}"
  loop_control:
    index_var: loop_idx
  tags:
    - exporter
    - service
