DOCUMENTATION = """
  lookup: chef_databag_items
  author: Ahmad Sherif <ahmad@gitlab.com>
  version_added: "2.9"
  short_description: Looks up the items of Chef data bag
  description:
      - Returns a list of Chef data bag items
  options:
    databag:
      description: Chef data bag name(s)
      required: True
    query:
      description: Chef search query, defaults to '*:*'
      required: False
  notes:
    - Requires KNIFE_RB_FILE environment variable to be set to the path of knife.rb
"""

EXAMPLES = """
---
- hosts: 127.0.0.1
  tasks:
    - name: Lookup Chef users
      debug: msg="{{ item }}"
      loop: "{{ query('chef_databag_items', databag='users', query='action:create') }}"
"""

RETURN = """
  _list:
    description: Requested value(s)
"""

import chef
from os import getenv
from ansible.plugins.lookup import LookupBase
from ansible.errors import AnsibleError

class LookupModule(LookupBase):
    def run(self, _terms, databag=None, query='*:*', **kwargs):
        """
        :arg databag: Name of Chef data bag to return items for
        :arg query: Chef search query
        """

        api_init_error = AnsibleError("Failed to initialize Chef, make sure KNIFE_RB_FILE environment variable is set correctly.")
        items = []

        try:
            chef_api = chef.ChefAPI.from_config_file(getenv('KNIFE_RB_FILE'))
        except:
            raise api_init_error

        if chef_api is None:
            raise api_init_error

        for item in chef.Search(databag, query):
            items.append(dict(item['raw_data']))

        return items
