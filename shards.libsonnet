// Environment defitions
// ------------------------
// Environments correspond to GCP projects and require manual steps to create, see
// Warning: You must complete some manual steps before creating a new environment!
// See https://gitlab.com/gitlab-com/gl-infra/ansible-workloads/db-provisioning#creating-a-new-environment
//
//  ansible_user: The SSH user associated with the terraform-ci service account (see README)
//  project_name: Project associated with the environment

local baseEnvPlays = [
  "consul",
  "bastion",
];

local baseEnvSecrets = [
  "consul_encrypt",
  "consul_ca",
  "consul_server_cert",
  "consul_server_key",
  "consul_client_cert",
  "consul_client_key",
];

local baseEnv = {
  plays: baseEnvPlays,
  secrets: baseEnvSecrets,
  consul_datacenter: 'gitlab_consul',
  cluster_env: true, // Whether or not to provision the cluster environment resources
  existing_vpc_name: "",
  existing_hosts: {
    // Format should be:
    // <group-a>: {
    //   label: "<label-name-a>",
    //   value: "<label-value-a>",
    // },
    // <group-b>: {
    //   label: "<label-name-b>",
    //   value: "<label-value-b>",
    // },
    // ...
  },
};

local envs = {
  environments: {
    gprd: baseEnv + {
      ansible_user: "sa_110000294032242666645",
      project_name: "gitlab-production-db",
      internal_subnet: "10.177.0.0/16",
      existing_vpc_name: "gprd",
      bastion_lb_addr_name: "gprd-gcp-tcp-lb-bastion",
    },
    gstg: baseEnv + {
      ansible_user: "sa_103468254774615126061",
      project_name: "gitlab-staging-db",
      internal_subnet: "10.176.0.0/16",
      existing_vpc_name: "gstg",
      bastion_lb_addr_name: "gstg-gcp-tcp-lb-bastion",
    },
    sandbox: baseEnv + {
      ansible_user: "sa_108276009635148287359",
      project_name: "gitlab-sandbox-db",
      internal_subnet: "10.178.0.0/16",
      // Sandbox has a single rails server that is used
      // as a sandbox for the Sharding team
      plays: baseEnvPlays + ["rails"],
      bastion_lb_addr_name: "sandbox-gcp-tcp-lb-bastion",
    },
    "sb-alpha": baseEnv + {
      ansible_user: "sa_101216133629974357326",
      project_name: "gitlab-sb-db-alpha",
      internal_subnet: "10.178.0.0/16",
      bastion_lb_addr_name: "sb-alpha-gcp-tcp-lb-bastion",
    },
    "sb-beta": baseEnv + {
      ansible_user: "sa_113614931821039347742",
      project_name: "gitlab-sb-db-beta",
      internal_subnet: "10.178.0.0/16",
      bastion_lb_addr_name: "sb-beta-gcp-tcp-lb-bastion",
    },
    "sb-gamma": baseEnv + {
      ansible_user: "sa_106400480090296922074",
      project_name: "gitlab-sb-db-gamma",
      internal_subnet: "10.178.0.0/16",
      bastion_lb_addr_name: "sb-gamma-gcp-tcp-lb-bastion",
    },
    "db-benchmarking": baseEnv + {
      ansible_user: "sa_113906828506893155477",
      project_name: "gitlab-db-benchmarking",
      internal_subnet: "10.255.18.0/24",
      bastion_lb_addr_name: "db-benchmarking-gcp-tcp-lb-bastion",
      consul_datacenter: "east-us-2",
      cluster_env: false,
      existing_vpc_name: "db-benchmarking",
      existing_hosts: {
        bastion: {
          label: "service",
          value: "bastion",
        },
        consul: {
          label: "service",
          value: "consul",
        },
      },
    },
  },
};

// Shard defintion
// ---------------------
// Shards are associated to environments and correspond to a set of CI jobs that
// configure them using Ansible
//
// After modifying this file you must run `make generate-config`

local baseShardPlays = [
  "postgres",
  "pgbouncer",
  "pgbouncer-sidekiq",
];

local baseShardSecrets = [
  "postgres_password",
  "pgbouncer_password",
  "patroni_password",
  "consul_postgresql_password",
];

local shards = {
  shards: {
    main: {
      environments: ["gprd", "gstg", "sandbox", "sb-alpha", "sb-beta", "sb-gamma"],
      plays: baseShardPlays,
      secrets: baseShardSecrets,
    },
    ci: {
      environments: ["sandbox", "sb-alpha", "sb-beta", "sb-gamma"],
      plays: baseShardPlays,
      secrets: baseShardSecrets,
    },
    events: {
      environments: ["db-benchmarking"],
      plays: baseShardPlays,
      secrets: baseShardSecrets,
    },
  },
};

envs + shards
