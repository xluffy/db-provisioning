local ci = import '.ci.jsonnet';
local shards = import 'shards.libsonnet';
local gcpGroupsFilter(groups) =
  local filters = std.map(function(group) "(labels.%s = %s)" % [group.label, group.value], std.objectValues(groups));
  if std.length(filters) > 0 then " OR " + std.join(" OR ", filters) else "";
local gcpGroups(group, data) = "labels.%s == '%s'" % [data.label, data.value];
local region = "us-east1";
local clusterGCPConfig(env, shard="") = {
  plugin: "gcp_compute",
  projects: [shards.environments[env].project_name],
  filters: [
    local filter = "(labels.gitlab_node_prefix = %s)" % env + gcpGroupsFilter(shards.environments[env].existing_hosts);
    if shard == "" then filter else filter + " OR (labels.gitlab_node_prefix = %s)" % shard,
  ],
  [if std.length(shards.environments[env].existing_hosts) > 0 then "groups"]: std.mapWithKey(gcpGroups, shards.environments[env].existing_hosts),
  keyed_groups: [
    { key: "labels.gitlab_node_type", separator: '' },
    { key: "labels.gitlab_node_level", separator: '' },
  ],
  scopes: ["https://www.googleapis.com/auth/compute"],
  // List host by name instead of the default public ip
  hostnames: ["name"],
  // Set an inventory parameter to use the Public IP address to connect to the host
  // For Public ip use "networkInterfaces[0].accessConfigs[0].natIP"
  compose: [{ ansible_host: "networkInterfaces[0].networkIP" }],
  auth_kind: "serviceaccount",
  regions: [region],
};
local clusterBaseConfig = {
  region: "${var.region}",
  service_account_ci: "${var.service_account_ci}",
  vpc: "${local.vpc}",
  subnet: "${google_compute_subnetwork.subnetwork.self_link}",
};

// GitLab CI
{
  ".gitlab-ci.json": {
                       // Default to branch pipelines because code is mirrored on ops where there are not
                       // any MRs
                       include: [
                         { template: "Workflows/Branch-Pipelines.gitlab-ci.yml" },
                       ],

                       stages: [
                         "Image",
                         "Prepare",
                         "Environments",
                         "Check Remote",
                       ],
                     }
                     + ci.imageCIJob("ansible")
                     + ci.imageCIJob("tf")
                     + ci.imageCIJob("lint")
                     + ci.notifyMirrorSourceJob()
                     + ci.lintCIJob()
                     + ci.checkRemoteJob()
                     + ci.environmentJobs(),
} + {
  ["ci/%s.json" % env]: {
                          stages: [
                                    env,
                                  ] +
                                  [
                                    "Shard:%s" % shard
                                    for shard in std.objectFields(shards.shards)
                                  ] +
                                  [
                                    "Prepare",
                                    "Deploy",
                                    "Destroy",
                                  ],
                        }
                        + ci.ansibleJobs(env)
                        + ci.tfPrepareJob(env)
                        + ci.tfApplyJob(env)
                        + ci.tfDestroyJob(env)
  for env in std.objectFields(shards.environments)
} + {
  // Ansible configuration GCP
  ["ansible/environments/%s/shards/%s/inventory/cluster.gcp.json" % [env, shard]]: clusterGCPConfig(env, shard)
  for env in std.objectFields(shards.environments)
  for shard in std.objectFields(shards.shards)
  if [shardEnv for shardEnv in shards.shards[shard].environments if shardEnv == env] != []
} + {
  // Ansible configuration vars
  ["ansible/environments/%s/shards/%s/inventory/vars.json" % [env, shard]]: {
    all: {
      vars: {
        shard: shard,
        env: env,
        ansible_user: shards.environments[env].ansible_user,
        ansible_ssh_private_key_file: "{{ lookup('env', 'ANSIBLE_SSH_PRIVATE_KEY_FILE') }}",
        cloud_provider: "gcp",
        project_name: shards.environments[env].project_name,

        // Bastion configuration
        bastion_ip: "{{ lookup('cached', 'gcp_address', '%s', region='%s') if 'bastion' in groups else '' }}" % [shards.environments[env].bastion_lb_addr_name, region],
        // StrictHostKeyChecking=no needed for CI. See https://gitlab.com/gitlab-com/gl-infra/ansible-workloads/db-provisioning/-/merge_requests/22/#note_638896128
        bastion_proxy_command: '-o ProxyCommand="ssh -o StrictHostKeyChecking=no -W %h:%p -i {{ ansible_ssh_private_key_file }} {{ ansible_user }}@{{ bastion_ip }}"',
        ansible_ssh_common_args: "{{ bastion_proxy_command if bastion_ip != '' else '' }}",
        consul_datacenter: shards.environments[env].consul_datacenter,
      } + {
        [secret]: "{{ lookup('gsm', '%s-%s-%s') }}" % [env, shard, secret]
        for secret in shards.shards[shard].secrets
      } + {
        [secret]: "{{ lookup('gsm', '%s-%s') }}" % [env, secret]
        for secret in shards.environments[env].secrets
      },
    },
  }
  for env in std.objectFields(shards.environments)
  for shard in std.objectFields(shards.shards)
  if [shardEnv for shardEnv in shards.shards[shard].environments if shardEnv == env] != []
} + {
  // Terraform environment configuration
  ["terraform/environments/%s/%s.tf.json" % [env, env]]: {
    "//": 'Generated by `make generate-config`, do not edit!!',
    module: {
      [if shards.environments[env].cluster_env then "cluster_env"]: clusterBaseConfig + {
        environment: env,
        project: shards.environments[env].project_name,
        source: "../../modules/cluster_env",
      },
      secrets: {
        environment: env,
        project: shards.environments[env].project_name,
        secrets: shards.environments[env].secrets,
        service_account_ci: "${var.service_account_ci}",
        source: "../../modules/secrets",
      },
    },
    variable: {
      project: { default: shards.environments[env].project_name },
      internal_subnet: { default: shards.environments[env].internal_subnet },
      environment: { default: env },
      existing_vpc_name: { default: shards.environments[env].existing_vpc_name },
    },
  }
  for env in std.objectFields(shards.environments)
} + {
  // Terraform shard configuration
  ["terraform/environments/%s/%s.tf.json" % [env, shard]]: {
    "//": 'Generated by `make generate-config`, do not edit!!',
    module: {
      [shard]: clusterBaseConfig + {
        name: shard,
        environment: env,
        zone: "${var.zone}",
        project: shards.environments[env].project_name,
        secrets: shards.shards[shard].secrets,
        source: "../../modules/cluster",
      },
    },
  }
  for env in std.objectFields(shards.environments)
  for shard in std.objectFields(shards.shards)
  if [shardEnv for shardEnv in shards.shards[shard].environments if shardEnv == env] != []
} + {
  // pgbouncer group variables
  ["ansible/environments/%s/shards/%s/inventory/group_vars/pgbouncer.json" % [env, shard]]: {
    pgbouncer_ini: {
      users: {
        pgbouncer: "md5{{ (pgbouncer_password + 'pgbouncer') | md5 }}",
      },
      databases: {
        gitlabhq_production: {
          host: "master.%s-postgresql.service.consul" % shard,
          port: 5432,
          pool_size: 40,
          auth_user: "pgbouncer",
        },
      },
    },
    allowed_user_groups: [
      env,
      'database',
    ],
    openssh_allow_groups: "{{ allowed_user_groups }}",
    sudo_groups: "{{ allowed_user_groups }}",
  }
  for env in std.objectFields(shards.environments)
  for shard in std.objectFields(shards.shards)
  if [shardEnv for shardEnv in shards.shards[shard].environments if shardEnv == env] != []
} + {
  // pgbouncer_sidekiq group variables
  ["ansible/environments/%s/shards/%s/inventory/group_vars/pgbouncer_sidekiq.json" % [env, shard]]: {
    pgbouncer_ini: {
      users: {
        pgbouncer: "md5{{ (pgbouncer_password + 'pgbouncer') | md5 }}",
      },
      databases: {
        gitlabhq_production_sidekiq: {
          host: "master.%s-postgresql.service.consul" % shard,
          port: 5432,
          pool_size: 15,
          auth_user: "pgbouncer",
          dbname: "gitlabhq_production",
        },
      },
    },
    allowed_user_groups: [
      env,
      'database',
    ],
    openssh_allow_groups: "{{ allowed_user_groups }}",
    sudo_groups: "{{ allowed_user_groups }}",
  }
  for env in std.objectFields(shards.environments)
  for shard in std.objectFields(shards.shards)
  if [shardEnv for shardEnv in shards.shards[shard].environments if shardEnv == env] != []
} + {
  // postgres group variables
  ["ansible/environments/%s/shards/%s/inventory/group_vars/postgres.json" % [env, shard]]: {
    gitlab_rb_patroni: {
      scope: "%s-postgresql" % shard,
    },
    pgbouncer_consul_service_name: "%s-db-replica" % shard,
    pgbouncer_consul_extra_checks: [
      {
        http: "http://0.0.0.0:8008/replica",
        interval: "2s",
      },
    ],
    allowed_user_groups: [
      env,
      'database',
    ],
    openssh_allow_groups: "{{ allowed_user_groups }}",
    sudo_groups: "{{ allowed_user_groups }}",
    walg_env_WALG_GS_PREFIX: "gs://gitlab-%s-postgres-backup/%s" % [env, shard],
    walg_env_GCS_NORMALIZE_PREFIX: false,
    walg_env_GCS_CONTEXT_TIMEOUT: 600,
  }
  for env in std.objectFields(shards.environments)
  for shard in std.objectFields(shards.shards)
  if [shardEnv for shardEnv in shards.shards[shard].environments if shardEnv == env] != []
} + {
  // bastion group variables
  ["ansible/environments/%s/shards/%s/inventory/group_vars/bastion.json" % [env, shard]]: {
    allowed_user_groups: [
      "database",
      "%s-bastion-only" % env,
    ],
    openssh_allow_groups: "{{ allowed_user_groups }}",
    sudo_include_sudoers_d: false,
    sudo_groups: [],
  }
  for env in std.objectFields(shards.environments)
  for shard in std.objectFields(shards.shards)
  if [shardEnv for shardEnv in shards.shards[shard].environments if shardEnv == env] != []
} + {
  // Ansible configuration for the environment hosts
  ["ansible/environments/%s/inventory/cluster.gcp.json" % env]: clusterGCPConfig(env)
  for env in std.objectFields(shards.environments)
} + {
  // Ansible configuration vars for the environment hosts
  ["ansible/environments/%s/inventory/vars.json" % env]: {
    all: {
      vars: {
        env: env,
        ansible_user: shards.environments[env].ansible_user,
        ansible_ssh_private_key_file: "{{ lookup('env', 'ANSIBLE_SSH_PRIVATE_KEY_FILE') }}",
        cloud_provider: "gcp",
        project_name: shards.environments[env].project_name,

        // Bastion configuration
        bastion_ip: "{{ lookup('cached', 'gcp_address', '%s', region='%s') if 'bastion' in groups else '' }}" % [shards.environments[env].bastion_lb_addr_name, region],
        // StrictHostKeyChecking=no needed for CI. See https://gitlab.com/gitlab-com/gl-infra/ansible-workloads/db-provisioning/-/merge_requests/22/#note_638896128
        bastion_proxy_command: '-o ProxyCommand="ssh -o StrictHostKeyChecking=no -W %h:%p -i {{ ansible_ssh_private_key_file }} {{ ansible_user }}@{{ bastion_ip }}"',
        ansible_ssh_common_args: "{{ bastion_proxy_command if bastion_ip != '' else '' }}",
        consul_datacenter: shards.environments[env].consul_datacenter,
      } + {
        [secret]: "{{ lookup('gsm', '%s-%s') }}" % [env, secret]
        for secret in shards.environments[env].secrets
      },
    },
  }
  for env in std.objectFields(shards.environments)
}
